<?php

namespace App\Controller;

use App\Entity\Concert;
use App\Entity\Email;
use App\Form\NewsletterType;
use App\Repository\ConcertRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="default")
     */
    public function concerts(ConcertRepository $cr)
    {
        //$concerts=$this->getDoctrine()->getRepository(Concert::class)->concertsAVenir();
        //$concertsPasses=$this->getDoctrine()->getRepository(Concert::class)->concertsPasses();
        $concerts=$cr->concertsAVenir();
        $concertsPasses=$cr->concertsPasses();
        return $this->render('default/concerts.html.twig', [
            'concert'=>$concerts,
            'concertsPasses' => $concertsPasses,

        ]);
    }


    /**
     * @Route("/concert/{id}", name="concert")
     */
    public function voirConcert(Concert $concert)
    {
        return $this->render('default/concert.html.twig', [
            'concert'=>$concert

        ]);
    }


    /**
     * @Route("/newsletter", name="newsletter")
     */
    public function formNewsletter(Request $request) {
        $email=new Email();
        $form=$this->createForm(NewsletterType::class,$email);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($email);
            $em->flush();
            return $this->redirectToRoute('default');
        }
        return $this->render('default/_formNewsletter.html.twig',['formObject' => $form]);
    }


}
